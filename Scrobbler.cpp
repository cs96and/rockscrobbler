/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#include "Scrobbler.h"
#include "LogWindow.h"
#include "Version.h"

#include <QCryptographicHash>
#include <QDateTime>
#include <QFile>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QMap>
#include <QRegularExpression>
#include <QSslConfiguration>
#include <QThread>
#include <QTimer>
#include <QUrl>
#include <QUrlQuery>
#include <QDomDocument>

#include <assert.h>

namespace
{
const QString BASE_URL(QStringLiteral("ws.audioscrobbler.com/2.0/"));
const QUrl SCROBBLE_URL(QStringLiteral("http://") + BASE_URL);
const QUrl SCROBBLE_URL_HTTPS(QStringLiteral("https://") + BASE_URL);
const QString API_KEY(QStringLiteral("5a32ff3f835bf5b06ad342c61b26e19f"));
const QByteArray API_SECRET(QByteArrayLiteral("7fd52cca5a68ee085f2fddd3f3c4f6cf"));
}

Scrobbler::Scrobbler(LogWindow &logWindow) :
	QObject(&logWindow),
	m_logWindow(logWindow),
	m_pNetworkAccessManager(new QNetworkAccessManager(this)),
	m_pScrobbleTimer(new QTimer(this)),
	m_nScrobbled(0),
	m_nIgnored(0),
	m_nErrors(0)
{
	// Set up scrobble timer
	m_pScrobbleTimer->setSingleShot(true);
	m_pScrobbleTimer->setInterval(10000);
	connect(m_pScrobbleTimer, &QTimer::timeout, this, &Scrobbler::onScrobbleTimeout);
}

Scrobbler::~Scrobbler()
{
}

void Scrobbler::authenticate(const QString &username, const QString &password)
{
	ParametersList parameters;
	parameters.push_back(Parameter(QStringLiteral("username"), username));
	parameters.push_back(Parameter(QStringLiteral("password"), password));

	QNetworkReply * const pAuthGetTokenReply = sendPostRequest(QStringLiteral("auth.getMobileSession"), parameters, REQUEST_OPTION_USE_HTTPS);
	connect(pAuthGetTokenReply, &QNetworkReply::finished, this, &Scrobbler::onAuthGetSessionReply);
}

void Scrobbler::requestUserInfo(const QString &username) const
{
	ParametersList parameters;
	parameters.push_back(Parameter(QStringLiteral("user"), username));

	QNetworkReply * const pGetUserReply = sendGetRequest(QStringLiteral("user.getInfo"), parameters, REQUEST_OPTION_NONE);
	connect(pGetUserReply, &QNetworkReply::finished, this, &Scrobbler::onGetUserInfoReply);
}

void Scrobbler::logout()
{
	m_sessionKey.clear();
	m_currentAvatar = QPixmap();
	m_userUrl.clear();

	emit loggedOut();
}

void Scrobbler::scrobbleFile(const QString &path, const uint tracksPerScrobble)
{
	if (!m_scrobbleRequestMap.empty())
	{
		emit scrobbleError(tr("Outstanding scrobble request still in progress"));
		return;
	}

	QFile scrobbleFile(path);
	if (!scrobbleFile.open(QIODevice::ReadOnly))
	{
		emit scrobbleError(tr("Failed to open %1").arg(path));
		return;
	}

	bool convertToUtc = true;

	try
	{
		QString headerLine, timezoneLine, clientLine;

		headerLine = readLineHelper(scrobbleFile);
		timezoneLine = readLineHelper(scrobbleFile);
		clientLine = readLineHelper(scrobbleFile);

		// Check if timestamps are already in UTC
		const QStringList timezoneFields = timezoneLine.split(QChar('/'));
		if ((timezoneFields.size() >= 2) && (timezoneFields[1].toUpper() == QLatin1String("UTC")))
			convertToUtc = false;
	}
	catch (const ReadLineException)
	{
		emit scrobbleError(tr("Failed to parse %1").arg(path));
		return;
	}

	ParametersList parameters;
	parameters.reserve(tracksPerScrobble * 7); // Maximum of 50 tracks, with 7 fields each.

	m_nScrobbled = m_nIgnored = m_nErrors = 0;

	bool requestSent = false;
	quint32 i = 0;

	try
	{
		while (!scrobbleFile.atEnd())
		{
			for (i = 0; i < tracksPerScrobble; /* NULL */)
			{
				const QStringList fields = readLineHelper(scrobbleFile).split(QChar('\t'));

				if ((fields.size() >= 7) && (fields[5] == "L"))
				{
					if (fields[0].isEmpty() || fields[2].isEmpty() || fields[6].isEmpty())
						continue;

					parameters.push_back(Parameter(QStringLiteral("artist[%1]").arg(i), fields[0]));
					if (!fields[1].isEmpty())
						parameters.push_back(Parameter(QStringLiteral("album[%1]").arg(i), fields[1]));
					parameters.push_back(Parameter(QStringLiteral("track[%1]").arg(i), fields[2]));
					if (!fields[3].isEmpty())
						parameters.push_back(Parameter(QStringLiteral("trackNumber[%1]").arg(i), fields[3]));
					if (!fields[4].isEmpty())
						parameters.push_back(Parameter(QStringLiteral("duration[%1]").arg(i), fields[4]));

					if (convertToUtc)
					{
						const uint localTimestamp = fields[6].toUInt();
						const uint utcTimestamp = convertTimestampToUtc(localTimestamp);

						const QString utcTimeString = QString::number(utcTimestamp);
						parameters.push_back(Parameter(QStringLiteral("timestamp[%1]").arg(i), utcTimeString));
					}
					else
					{
						parameters.push_back(Parameter(QStringLiteral("timestamp[%1]").arg(i), fields[6]));
					}

					if ((fields.size() > 7) && !fields[7].isEmpty())
						parameters.push_back(Parameter(QStringLiteral("mbid[%1]").arg(i), fields[7]));

					++i;
				}
			}

			if (!parameters.isEmpty())
			{
				QNetworkReply * const pReply = sendPostRequest(QStringLiteral("track.scrobble"), parameters, REQUEST_OPTION_USE_AUTH);
				connect(pReply, &QNetworkReply::finished, this, &Scrobbler::onScrobbleReply);
				m_scrobbleRequestMap.insert(pReply, ScrobbleRequest(parameters, i));

				parameters.clear();
				requestSent = true;

				QThread::msleep(200);
			}
		}
	}
	catch (const EndOfFile)
	{
		if (!parameters.isEmpty())
		{
			QNetworkReply * const pReply = sendPostRequest(QStringLiteral("track.scrobble"), parameters, REQUEST_OPTION_USE_AUTH);
			connect(pReply, &QNetworkReply::finished, this, &Scrobbler::onScrobbleReply);
			m_scrobbleRequestMap.insert(pReply, ScrobbleRequest(parameters, i));

			requestSent = true;
		}
	}
	catch (const ReadLineError)
	{
		emit scrobbleError(tr("Failed to read %1").arg(path));
	}

	// Start the scrobble timer
	if (requestSent)
		m_pScrobbleTimer->start();
}

void Scrobbler::onAuthGetSessionReply()
{
	const QDomElement root = parseResponse(sender(), &Scrobbler::authenticationError);

	if (root.attribute(QStringLiteral("status")) == QLatin1String("ok"))
	{
		const QDomElement sessionElement = root.firstChildElement(QStringLiteral("session"));
		if (!sessionElement.isNull())
		{
			const QString username = sessionElement.firstChildElement(QStringLiteral("name")).text();
			m_sessionKey = sessionElement.firstChildElement(QStringLiteral("key")).text();

			if (m_sessionKey.isEmpty())
				emit authenticationError(tr("Failed to parse server response, 'key' node not found"));
			else
				emit authenticateSuccess(username);
		}
		else
		{
			emit authenticationError(tr("Failed to parse server response: 'session' node not found"));
		}
	}
	else
	{
		emit authenticationError(tr("auth.getSession status not OK"));
	}
}

void Scrobbler::onGetUserInfoReply()
{
	const QDomElement root = parseResponse(sender(), 0);

	if (root.attribute(QStringLiteral("status")) == QLatin1String("ok"))
	{
		const QDomElement userElement = root.firstChildElement(QStringLiteral("user"));
		if (!userElement.isNull())
		{
			// Find the user url
			m_userUrl = userElement.firstChildElement(QStringLiteral("url")).text();

			// Find the image  node
			const QDomNodeList imageNodes = userElement.elementsByTagName(QStringLiteral("image"));

			// Find the medium size image.
			for (int i = 0; i < imageNodes.length(); ++i)
			{
				const QDomNode node = imageNodes.item(i);
				const QDomNode sizeNode = node.attributes().namedItem(QStringLiteral("size"));

				if (sizeNode.nodeValue() == QLatin1String("medium"))
				{
					const QString imageUrl = node.toElement().text().trimmed();

					if (!imageUrl.isEmpty())
					{
						QNetworkRequest request(imageUrl);
						request.setRawHeader(QByteArrayLiteral("User-Agent"), QByteArrayLiteral("RockScrobbler " ROCK_SCROBBLER_VERSION));

						QNetworkReply * const pGetAvatarReply = m_pNetworkAccessManager->get(request);
						connect(pGetAvatarReply, &QNetworkReply::finished, this, &Scrobbler::onGetAvatarReply);
						return;
					}
				}
			}
		}
	}

	// No avatar found, emit a null pixmap.
	m_currentAvatar = QPixmap();
	emit avatar(m_currentAvatar);
}

void Scrobbler::onGetAvatarReply()
{
	// Ensure reply gets deleted later
	sender()->deleteLater();

	QNetworkReply * const pNetworkReply = dynamic_cast<QNetworkReply *>(sender());
	assert(nullptr != pNetworkReply);

	if (pNetworkReply->error() == QNetworkReply::NoError)
	{
		m_currentAvatar.loadFromData(pNetworkReply->readAll());
	}
	else
	{
		m_currentAvatar = QPixmap();
		m_logWindow.appendToLog(tr("Failed to retreive avatar from %1: %2").arg(pNetworkReply->url().toString()).arg(pNetworkReply->errorString()));
	}

	emit avatar(m_currentAvatar);
}

void Scrobbler::onScrobbleReply()
{
	const QDomElement root = parseResponse(sender(), &Scrobbler::scrobbleError);

	// Lookup the request in the map of outstanding requests
	quint32 nScrobbleTracksRequested = 0;
	QNetworkReply * const pNetworkReply = dynamic_cast<QNetworkReply *>(sender());
	const auto iter = m_scrobbleRequestMap.find(pNetworkReply);

	if (m_scrobbleRequestMap.end() != iter)
		nScrobbleTracksRequested = iter.value().m_nTracks;

	if (root.attribute(QStringLiteral("status")) == QLatin1String("ok"))
	{
		const QDomElement scrobblesElement = root.firstChildElement(QStringLiteral("scrobbles"));
		if (!scrobblesElement.isNull())
		{
			m_nScrobbled += scrobblesElement.attribute(QStringLiteral("accepted")).toUInt();
			m_nIgnored += scrobblesElement.attribute(QStringLiteral("ignored")).toUInt();

			// Loop through the scrobbled data and append to log
			const QDomNodeList scrobbleList = scrobblesElement.elementsByTagName(QStringLiteral("scrobble"));
			for (int i = 0; i < scrobbleList.length(); ++i)
			{
				const QDomNode scrobbleNode = scrobbleList.item(i);

				QString artist = scrobbleNode.firstChildElement(QStringLiteral("artist")).text();
				QString track = scrobbleNode.firstChildElement(QStringLiteral("track")).text();

				// If the artist or track names contain UTF-8 characters, then Last.fm send them as individual bytes in the XML response.
				// E.g. "&#227;&#129;&#138;&#227;&#130;&#130;&#227;&#129;&#166;&#227;&#129;&#170;&#227;&#129;&#151;"
				// The QString returned from the text() call above will have converted these into individual characters,
				// rather than decoding multiple bytes into single characters.

				// Convert the strings into QByteArrays using toLatin1(), then read them back with fromUtf8().
				// The QString will then contain the correctly decoded data.

				artist = QString::fromUtf8(artist.toLatin1());
				track = QString::fromUtf8(track.toLatin1());

				const QDomElement ignoredMessageElement = scrobbleNode.firstChildElement(QStringLiteral("ignoredMessage"));
				const bool isIgnored = (0 != ignoredMessageElement.attribute(QStringLiteral("code")).toUInt());

				if (isIgnored)
					m_logWindow.appendToLog(tr("Ignored %1 - %2 (%3)").arg(artist).arg(track).arg(ignoredMessageElement.text()));
				else
					m_logWindow.appendToLog(tr("Scrobbled %1 - %2").arg(artist).arg(track));
			}
		}
		else
		{
			m_logWindow.appendToLog(tr("Failed to parse scrobble response: 'scrobbles' element not found"));
			m_nErrors += nScrobbleTracksRequested;
		}
	}
	else
	{
		QDomElement errorRoot = root.firstChildElement(QStringLiteral("error"));

		m_logWindow.appendToLog(errorRoot.text());

		switch (errorRoot.attribute(QStringLiteral("code")).toUInt())
		{
			// From http://www.last.fm/api/errorcodes
			case 8:		// Operation failed - Most likely the backend service failed. Please try again.
			case 11:	// Service Offline - This service is temporarily offline. Try again later.
			case 16:	// The service is temporarily unavailable, please try again.
				if ((m_scrobbleRequestMap.end() != iter) && (++(iter.value().m_nFailedAttempts) < 3))
				{
					// Retry the request in 5 seconds.
					m_retryQueue.enqueue(iter.value());
					QTimer::singleShot(5000, this, SLOT(onRetryRequest()));
					break;
				}
				// else fall through

			default:
				m_nErrors += nScrobbleTracksRequested;
				break;
		}
	}

	if (m_scrobbleRequestMap.end() != iter)
		m_scrobbleRequestMap.erase(iter);

	if (m_scrobbleRequestMap.empty() && m_retryQueue.empty())
	{
		m_pScrobbleTimer->stop();

		emit scrobbleComplete(m_nScrobbled, m_nIgnored, m_nErrors);
	}
	else
	{
		// Re-start the timer
		m_pScrobbleTimer->start();
	}
}

void Scrobbler::onRetryRequest()
{
	m_logWindow.appendToLog(tr("Retrying request"));

	ScrobbleRequest retryRequest = m_retryQueue.dequeue();

	QNetworkReply * const pReply = sendPostRequest(QStringLiteral("track.scrobble"), retryRequest.m_parameters, REQUEST_OPTION_USE_AUTH);
	connect(pReply, &QNetworkReply::finished, this, &Scrobbler::onScrobbleReply);

	m_scrobbleRequestMap.insert(pReply, retryRequest);

	// Re-start the timer
	m_pScrobbleTimer->start();
}

void Scrobbler::onScrobbleTimeout()
{
	quint32 nTimedOut = 0;
	for (const auto &request : m_scrobbleRequestMap)
		nTimedOut += request.m_nTracks;

	for (const auto &request : m_retryQueue)
		nTimedOut += request.m_nTracks;

	m_scrobbleRequestMap.clear();
	m_retryQueue.clear();

	m_nErrors += nTimedOut;

	m_logWindow.appendToLog(tr("Scrobble request timed out for %1 tracks").arg(nTimedOut));

	emit scrobbleComplete(m_nScrobbled, m_nIgnored, m_nErrors);
}

void Scrobbler::onSslError(const QList<QSslError> &errors)
{
	QString errorString(tr("SSL Error"));

	for (const auto &error : errors)
	{
		errorString.append(QChar('\n'));
		errorString.append(error.errorString());
	}

	emit scrobbleError(errorString);
}

QNetworkReply *Scrobbler::sendGetRequest(const QString &method, const ParametersList &parameters, const RequestOptions requestOptions) const
{
	QUrl url((0 != (requestOptions & REQUEST_OPTION_USE_HTTPS)) ? SCROBBLE_URL_HTTPS : SCROBBLE_URL);
	QUrlQuery query;
	query.addQueryItem(QStringLiteral("method"), method);

	ParametersList authParameters;
	authParameters.append(Parameter(QStringLiteral("api_key"), API_KEY));

	for (const auto &parameter : parameters)
		query.addQueryItem(parameter.first, parameter.second);

	for (const auto &parameter : authParameters)
		query.addQueryItem(parameter.first, parameter.second);

	if (0 != (requestOptions & REQUEST_OPTION_USE_AUTH))
		query.addQueryItem(QStringLiteral("api_sig"), signParameters(method, parameters, authParameters));

	url.setQuery(query);

	if (m_logWindow.isDebugLoggingEnabled())
	{
		const QString urlString = url.toString();
		m_logWindow.appendToLog(urlString);
	}

	QNetworkRequest request(url);
	request.setRawHeader(QByteArrayLiteral("User-Agent"), QByteArrayLiteral("RockScrobbler " ROCK_SCROBBLER_VERSION));

	return m_pNetworkAccessManager->get(request);
}

QNetworkReply *Scrobbler::sendPostRequest(const QString &method, const ParametersList &parameters, const RequestOptions requestOptions) const
{
	QUrlQuery postData;
	postData.addQueryItem(QStringLiteral("method"), method);

	ParametersList authParameters;
	authParameters.append(Parameter(QStringLiteral("api_key"), API_KEY));

	if (0 != (requestOptions & REQUEST_OPTION_USE_AUTH))
		authParameters.append(Parameter(QStringLiteral("sk"), m_sessionKey));

	for (const auto &parameter : parameters)
		postData.addQueryItem(parameter.first, parameter.second);

	for (const auto &parameter : authParameters)
		postData.addQueryItem(parameter.first, parameter.second);

	postData.addQueryItem(QStringLiteral("api_sig"), signParameters(method, parameters, authParameters));

	// encodedQuery() doesn't replace '+' with the %2B, because it presumes that it is used to represent a space.
	// We have not replaced any spaces with '+' so we can safely replace them all with %2B.
	QString postDataEncoded = postData.query(QUrl::FullyEncoded).replace(QChar('+'), QStringLiteral("%2B"));

	QNetworkRequest request((0 != (requestOptions & REQUEST_OPTION_USE_HTTPS)) ? SCROBBLE_URL_HTTPS : SCROBBLE_URL);
	request.setSslConfiguration(QSslConfiguration::defaultConfiguration());
	request.setRawHeader(QByteArrayLiteral("User-Agent"), QByteArrayLiteral("RockScrobbler " ROCK_SCROBBLER_VERSION));
	request.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/x-www-form-urlencoded"));

	QNetworkReply * const pReply = m_pNetworkAccessManager->post(request, postDataEncoded.toUtf8());

	connect(pReply, &QNetworkReply::sslErrors, this, &Scrobbler::onSslError);

	if (m_logWindow.isDebugLoggingEnabled())
	{
		// blank out password before logging
		postDataEncoded.replace(QRegularExpression(QStringLiteral("(&password=)[^&]*")), QStringLiteral("\\1***"));
		m_logWindow.appendToLog(postDataEncoded);
	}

	return pReply;
}

QString Scrobbler::signParameters(const QString &method, const ParametersList &parameters, const ParametersList &authParameters)
{
	typedef QMap<QString, QString> ParametersMap;

	ParametersMap parametersMap;
	parametersMap.insert(QStringLiteral("method"), method);

	// Convert the two parameter lists to a map so we get the parameters in alphabetical order.
	for (const auto &parameter : parameters)
		parametersMap.insert(parameter.first, parameter.second);

	for (const auto &parameter : authParameters)
		parametersMap.insert(parameter.first, parameter.second);

	// Ignore 'format' and 'callback' parameters
	parametersMap.remove(QStringLiteral("format"));
	parametersMap.remove(QStringLiteral("callback"));

	// Hash all the parameters in alphabetical order
	QCryptographicHash hash(QCryptographicHash::Md5);
	for (ParametersMap::const_iterator iter = parametersMap.cbegin(), iterEnd = parametersMap.cend(); iterEnd != iter; ++iter)
	{
		hash.addData(iter.key().toUtf8());
		hash.addData(iter.value().toUtf8());
	}

	// Add the API secret to the hash
	hash.addData(API_SECRET);

	return hash.result().toHex();
}

QDomElement Scrobbler::parseResponse(QObject * const pSender, ErrorSignalPtr pErrorSignal) const
{
	// Ensure reply gets deleted later
	pSender->deleteLater();

	QNetworkReply * const pNetworkReply = dynamic_cast<QNetworkReply *>(pSender);
	assert(nullptr != pNetworkReply);

	if (m_logWindow.isDebugLoggingEnabled())
	{
		const QByteArray xml = pNetworkReply->peek(0xfffffff);
		m_logWindow.appendToLog(QString::fromUtf8(xml));
	}

	QDomElement root;
	QDomDocument domDocument;
	QString parseErrorMessage;

	if (domDocument.setContent(pNetworkReply, false, &parseErrorMessage))
	{
		root = domDocument.documentElement();
		if (root.tagName() != QLatin1String("lfm"))
		{
			if (nullptr != pErrorSignal)
				emit (this->*(pErrorSignal))(tr("Failed to parse response:\n'lfm' node not found"));
			root.clear();
		}
	}
	else if (nullptr != pErrorSignal)
	{
		if (pNetworkReply->error() != QNetworkReply::NoError)
			emit (this->*(pErrorSignal))(pNetworkReply->errorString());
		else
			emit (this->*(pErrorSignal))(tr("Failed to parse response:\n%1").arg(parseErrorMessage));
	}

	return root;
}

QString Scrobbler::readLineHelper(QFile &file)
{
	QString line;
	do
	{
		QByteArray lineData = file.readLine();
		if (lineData.isEmpty())
		{
			if (file.atEnd())
				throw EndOfFile();
			else
				throw ReadLineError();
		}

		line = QString::fromUtf8(lineData).trimmed();
	}
	while (line.isEmpty());

	return line;
}

uint Scrobbler::convertTimestampToUtc(const uint timestamp)
{
	// The QDateTime::fromTime_t function assumes that the timestamp is in UTC.
	// However the timestamp in the .scrobbler.log is already in local time, so we need to convert it to UTC.

	// Get date/time.  The Qt::UTC flag keeps the resulting QDateTime in UTC.
	QDateTime localTime = QDateTime::fromTime_t(timestamp, Qt::UTC);

	// Override the timespec to local time, without converting the time.
	localTime.setTimeSpec(Qt::LocalTime);

	// Convert back to a time_t timestamp.
	return localTime.toTime_t();
}

Scrobbler::ScrobbleRequest::ScrobbleRequest(const ParametersList &parameters, const quint32 nTracks) :
	m_parameters(parameters),
	m_nTracks(nTracks),
	m_nFailedAttempts(0)
{
}

Scrobbler::ScrobbleRequest::ScrobbleRequest(const ScrobbleRequest &rhs) :
	m_parameters(rhs.m_parameters),
	m_nTracks(rhs.m_nTracks),
	m_nFailedAttempts(rhs.m_nFailedAttempts)
{
}

Scrobbler::ScrobbleRequest &Scrobbler::ScrobbleRequest::operator=(const ScrobbleRequest &rhs)
{
	if (this != &rhs)
	{
		m_parameters = rhs.m_parameters;
		m_nTracks = rhs.m_nTracks;
		m_nFailedAttempts = rhs.m_nFailedAttempts;
	}

	return *this;
}
