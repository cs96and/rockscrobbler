/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#ifndef SCROBBLER_H
#define SCROBBLER_H

#include <QByteArray>
#include <QDomElement>
#include <QList>
#include <QMap>
#include <QObject>
#include <QPair>
#include <QPixmap>
#include <QQueue>
#include <QSslError>
#include <QString>
#include <QUrl>

class QFile;
class QNetworkAccessManager;
class QNetworkReply;
class QTimer;
class LogWindow;

class Scrobbler : public QObject
{
	Q_OBJECT

public:
	/**
	 *	@brief	Constructor
	 */
	explicit Scrobbler(LogWindow &logWindow);

	/**
	 *	@brief	Destructor
	 */
	virtual ~Scrobbler() Q_DECL_OVERRIDE;

	/**
	 *	@brief	Get the current session key.
	 */
	const QString &getSessionKey() const;

	/**
	 *	@brief	Set the current session key.
	 */
	void setSessionKey(const QString &sessionKey);

	/**
	 *	@brief	Get the URL for the user's last.fm web page.
	 */
	const QUrl &getUserUrl() const;

	/**
	 *	@brief	Get the user's avatar.
	 */
	const QPixmap &getCurrentAvatar() const;

	/**
	 *	@brief	Return true if we are currently logged in.
	 */
	bool isLoggedIn() const;

signals:
	/**
	 *	@brief	Signal raised on failing to log in to last.fm
	 */
	void authenticationError(const QString &error) const;

	/**
	 *	@brief	Signal raised on successful login to last.fm
	 */
	void authenticateSuccess(const QString &username) const;

	/**
	 *	@brief	Signal raised when avatar is received.
	 */
	void avatar(const QPixmap &avatar) const;

	/**
	 *	@brief	Signal raised when logged out.
	 */
	void loggedOut() const;

	/**
	 *	@brief	Signal raised when an error occurred scrobbling tracks.
	 */
	void scrobbleError(const QString &error) const;

	/**
	 *	@brief	Signal raised on scrobble completed.
	 */
	void scrobbleComplete(quint32 nScrobbled, quint32 nIgnored, quint32 nErrors) const;

public slots:
	/**
	 *	@brief	Log in to last.fm.
	 */
	void authenticate(const QString &username, const QString &password);

	/**
	 *	@brief	Request retrieval of the user info of a given user.
	 *			The avatar is returned in the avatar signal.
	 */
	void requestUserInfo(const QString &username) const;

	/**
	 *	@brief	Log out from last.fm.
	 */
	void logout();

	/**
	 *	@brief	Scrobble data contained in a file.
	 */
	void scrobbleFile(const QString &path, const uint tracksPerScrobble);

private slots:
	/**
	 *	@brief	Process an auth.getMobileSession reply.
	 */
	void onAuthGetSessionReply();

	/**
	 *	@brief	Process a user.getInfo reply.
	 */
	void onGetUserInfoReply();

	/**
	 *	@brief	Process a get avatar image reply.
	 */
	void onGetAvatarReply();

	/**
	 *	@brief	Process a track.scrobble reply.
	 */
	void onScrobbleReply();

	/**
	 *	@brief	Retry the first queued request.
	 */
	void onRetryRequest();

	/**
	 *	@brief	Process a scrobble timeout.
	 */
	void onScrobbleTimeout();

	/**
	 *	@brief	Process an SSL error.
	 */
	void onSslError(const QList<QSslError> &errors);

private:
	typedef void (Scrobbler::*ErrorSignalPtr)(const QString &error) const;
	typedef QPair<QString, QString> Parameter;
	typedef QList<Parameter> ParametersList;

	struct ScrobbleRequest
	{
		ScrobbleRequest(const ParametersList &parameters, const quint32 nTracks);

		ScrobbleRequest(const ScrobbleRequest &rhs);

		ScrobbleRequest &operator=(const ScrobbleRequest &rhs);

		ParametersList	m_parameters;
		quint32			m_nTracks;
		quint32			m_nFailedAttempts;
	};

	typedef	QMap<QNetworkReply *, ScrobbleRequest> ScrobbleRequestMap;
	typedef QQueue<ScrobbleRequest> ScrobbleQueue;

	enum RequestOptions
	{
		REQUEST_OPTION_NONE      = 0x00,
		REQUEST_OPTION_USE_AUTH  = 0x01,
		REQUEST_OPTION_USE_HTTPS = 0x02
	};

	/**
	 *	Post a request to last.fm, using HTTP GET
	 */
	QNetworkReply *sendGetRequest(const QString &method, const ParametersList &parameters, const RequestOptions requestOptions) const;

	/**
	 *	Post a request to last.fm, using HTTP POST
	 */
	QNetworkReply *sendPostRequest(const QString &method, const ParametersList &parameters, const RequestOptions requestOptions) const;

	/**
	 *	@brief	Returns an MD5 signature of the parameters
	 */
	static QString signParameters(const QString &method, const ParametersList &parameters, const ParametersList &authParameters);

	/**
	 *	@brief	Initial parsing of last.fm XML response
	 */
	QDomElement parseResponse(QObject * const pSender, ErrorSignalPtr pErrorSignal) const;

	// Empty structs used as exceptions thrown from ReadLineHelper
	struct ReadLineException {};
	struct ReadLineError : public ReadLineException {};
	struct EndOfFile : public ReadLineException {};

	/**
	 *	@brief	Read a line from a file and throw an exception on error, or end of line.
	 *			Blank lines are also skipped.
	 */
	static QString readLineHelper(QFile &file);

	/**
	 *	@brief	Convert a time_t timestamp from localtime to UTC.
	 */
	static uint convertTimestampToUtc(const uint timestamp);

	LogWindow				&m_logWindow;
	QNetworkAccessManager	*m_pNetworkAccessManager;
	QString					m_sessionKey;
	QUrl					m_userUrl;
	QPixmap					m_currentAvatar;

	QTimer *				m_pScrobbleTimer;
	ScrobbleRequestMap		m_scrobbleRequestMap;
	ScrobbleQueue			m_retryQueue;
	quint32					m_nScrobbled;
	quint32					m_nIgnored;
	quint32					m_nErrors;
};

inline bool Scrobbler::isLoggedIn() const
{
	return !getSessionKey().isEmpty();
}

inline const QString &Scrobbler::getSessionKey() const
{
	return m_sessionKey;
}

inline const QUrl &Scrobbler::getUserUrl() const
{
	return m_userUrl;
}

inline const QPixmap &Scrobbler::getCurrentAvatar() const
{
	return m_currentAvatar;
}

inline void Scrobbler::setSessionKey(const QString &sessionKey)
{
	m_sessionKey = sessionKey;
}

#endif // SCROBBLER_H
