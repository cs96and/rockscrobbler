/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#ifndef LOGWINDOW_H
#define LOGWINDOW_H

#include <QMetaObject>
#include <QQueue>
#include <QScopedPointer>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSystemTrayIcon>

class BitBucketApi;
class DriveWatcher;
class Scrobbler;
class SettingsDialog;

class QAction;
class QTimer;
class QTimerEvent;

namespace Ui {
	class LogWindow;
}

class LogWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit LogWindow(QWidget *parent = 0);
	virtual ~LogWindow() Q_DECL_OVERRIDE;

	bool isDebugLoggingEnabled() const;

	static QString getCurrentExecutablePath();

public slots:
	void appendToLog(const QString &str);

private slots:
	void onDriveInserted(const QString &path);
	void onScrobbleTimer();
	void onManualScrobble();
	void onGotoLastFmPage();
	void onShowAbout();

	void onAuthenticateSuccess(const QString &username);
	void onAvatar(const QPixmap &avatar);
	void onLogout();
	void onScrobbleComplete(quint32 nScrobbled, quint32 nIgnored, quint32 nErrors);
	void onScrobblerError(const QString &error);
	void onUseAvatarAsIconChanged(bool useAvatarAsIcon);
	void onShouldAutoCheckForUpdatesChanged(bool shouldAutoCheckForUpdates);
	void onNewVersionAvailable(const QString &newVersion);

private:
	void backupFile(const QString path);

	virtual void timerEvent(QTimerEvent * pEvent) Q_DECL_OVERRIDE;

	QScopedPointer<Ui::LogWindow>		ui;
	QSystemTrayIcon						*m_pSystemTrayIcon;
	QAction								*m_pManualScrobbleAction;
	QAction								*m_pGotoLastFmProfileAction;
	DriveWatcher						*m_pDriveWatcher;
	Scrobbler							*m_pScrobbler;
	BitBucketApi						*m_pBitBucketApi;
	QScopedPointer<SettingsDialog>		m_pSettingsDialog;
	QQueue<QString>						m_driveInsertionQueue;
	QString								m_currentScrobbleFile;
	bool								m_isManualScrobble;
	QMetaObject::Connection				m_messageClickConnection;
	int									m_autoUpdateTimerId;
};

#endif // LOGWINDOW_H
