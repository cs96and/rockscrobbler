/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

	Rock Scrobbler is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Rock Scrobbler is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#ifndef BIT_BUCKET_API_H
#define BIT_BUCKET_API_H

#include <QObject>
#include <QUrl>

#include <functional>

class QNetworkAccessManager;

class BitBucketApi : public QObject
{
	Q_OBJECT

public:
	explicit BitBucketApi(QObject * parent);

	/**
	 *	@brief	Destructor
	 */
	virtual ~BitBucketApi() Q_DECL_OVERRIDE;

signals:
	/**
	 *	@brief	Signal raised when a new version is detected.
	 */
	void newVersionAvailable(const QString version);

	/**
	 *	@brief	Signal raised when an error occurs.
	 */
	void error(const QString errorMessage);

public slots:
	/**
	 *	@brief	Check for a new version
	 */
	void checkForUpdate();

private slots:
	/** 
	 *	@brief	Process a reply to a get tags request.
	 */
	void onGetTagsReply();

private:
	/** 
	 *	@brief	Functor to sort version numbers.
	 */
	struct VersionLess : public std::binary_function<QString, QString, bool>
	{
		bool operator()(const QString &lhs, const QString &rhs) const;
	};

	QNetworkAccessManager	*m_pNetworkAccessManager;
};

#endif // BIT_BUCKET_API_H
