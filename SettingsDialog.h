/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QPixmap>
#include <QScopedPointer>
#include <QString>
#include <QtWidgets/QDialog>

class QAbstractButton;
class QMenu;
class QSettings;
class Scrobbler;

namespace Ui {
	class SettingsDialog;
}

class SettingsDialog : public QDialog
{
	Q_OBJECT

public:
	explicit SettingsDialog(Scrobbler &scrobbler, QWidget *parent = 0);
	virtual ~SettingsDialog() Q_DECL_OVERRIDE;

	/**
	 *	@brief	Get the current user's username
	 */
	QString getUsername() const;

	/**
	 *	@brief	Get the amount of seconds to wait before scrobbling when a device in inserted.
	 */
	uint getWaitBeforeScrobble() const;

	/**
	 *	@brief	Should we use the user's avatar as the system tray icon?
	 */
	bool shouldUseAvatarAsIcon() const;

	/**
	 *	@brief	Should we automatically check for updates?
	 */
	bool shouldAutoCheckForUpdates() const;

	/**
	 *	@brief	Is debug logging enabled?
	 */
	bool isDebugLoggingEnabled() const;

	/**
	 *	@brief	Get the number of tracks to send in each scrobble request.
	 */
	uint getTracksPerScrobble() const;

signals:
	void useAvatarAsIconChanged(bool useAvatarAsIcon) const;
	void shouldAutoCheckForUpdatesChanged(bool shouldAutoCheckForUpdates) const;

private slots:
	void requestAvatar();
	void onLoginButtonClicked();
	void onLogout();
	void onAuthenticationError(const QString &error);
	void onAuthenticateSuccess();
	void onAuthenticateSuccess(const QString &username);
	void onAvatar(const QPixmap &avatar);
	void loadSettings();
	void saveSettings();

private:
	const QPixmap						m_defaultUserPixmap;

	QScopedPointer<Ui::SettingsDialog>	ui;
	QSettings							*m_pSettings;
	QSettings							*m_pRegistrySettings;
	Scrobbler							&m_scrobbler;
	QMenu								*m_pAvatarContextMenu;
};

#endif // SETTINGSDIALOG_H
