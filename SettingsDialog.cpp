/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#include "LogWindow.h"
#include "SettingsDialog.h"
#include "Scrobbler.h"
#include "ui_SettingsDialog.h"

#include <QSettings>
#include <QTimer>
#include <QtWidgets/QMenu>
#include <QtWidgets/QPushButton>

namespace
{
#if (defined QT_DEBUG)
const QString REGISTRY_KEY_NAME(QStringLiteral("RockScrobblerDebug"));
#else
const QString REGISTRY_KEY_NAME(QStringLiteral("RockScrobbler"));
#endif

const QString SETTING_USERNAME(QStringLiteral("username"));
const QString SETTING_PASSWORD(QStringLiteral("password"));
const QString SETTING_SESSION_KEY(QStringLiteral("sessionKey"));

const QString SETTING_USE_AVATAR_AS_ICON(QStringLiteral("useAvatarAsIcon"));
const QString SETTING_AUTO_CHECK_FOR_UPDATES(QStringLiteral("autoCheckForUpdates"));
const QString SETTING_ENABLE_DEBUG_LOGGING(QStringLiteral("enableDebugLogging"));
const QString SETTING_WAIT_BEFORE_SCROBBLE(QStringLiteral("waitBeforeScrobble"));
const QString SETTING_TRACKS_PER_SCROBBLE(QStringLiteral("tracksPerScrobble"));
}

SettingsDialog::SettingsDialog(Scrobbler &scrobbler, QWidget *parent) :
	QDialog(parent),
	m_defaultUserPixmap(QStringLiteral(":/icons/default_user_medium.png")),
	ui(new Ui::SettingsDialog),
	m_pSettings(new QSettings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral("Toastware"), REGISTRY_KEY_NAME, this)),
	m_pRegistrySettings(new QSettings(QStringLiteral("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run"), QSettings::NativeFormat, this)),
	m_scrobbler(scrobbler)
{
	ui->setupUi(this);

	// Create avatar label context menu
	m_pAvatarContextMenu = new QMenu(this);
	m_pAvatarContextMenu->addAction(tr("Refresh Avatar"), this, SLOT(requestAvatar()));
	ui->avatarLabel->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui->avatarLabel, &QLabel::customContextMenuRequested, [this](const QPoint &point) {
		m_pAvatarContextMenu->exec(mapToGlobal(point));
	});

	connect(ui->loginButton, &QPushButton::clicked, this, &SettingsDialog::onLoginButtonClicked);
	connect(ui->logoutButton, &QPushButton::clicked, &m_scrobbler, &Scrobbler::logout);
	connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &SettingsDialog::saveSettings);
	connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &SettingsDialog::loadSettings);
	connect(&m_scrobbler, &Scrobbler::authenticationError, this, &SettingsDialog::onAuthenticationError);
	connect(&m_scrobbler, &Scrobbler::authenticateSuccess, this, (void (SettingsDialog::*)(const QString &)) &SettingsDialog::onAuthenticateSuccess);
	connect(&m_scrobbler, &Scrobbler::loggedOut, this, &SettingsDialog::onLogout);
	connect(&m_scrobbler, &Scrobbler::avatar, this, &SettingsDialog::onAvatar);

	loadSettings();

	if (m_scrobbler.isLoggedIn())
	{
		QTimer::singleShot(0, this, SLOT(onAuthenticateSuccess()));
	}
	else
	{
		ui->loggedInWidget->hide();
		ui->loginButton->setDefault(true);
		show();
	}
}

SettingsDialog::~SettingsDialog()
{
}

QString SettingsDialog::getUsername() const
{
	return m_pSettings->value(SETTING_USERNAME).toString();
}

uint SettingsDialog::getWaitBeforeScrobble() const
{
	return m_pSettings->value(SETTING_WAIT_BEFORE_SCROBBLE).toUInt();
}

bool SettingsDialog::shouldUseAvatarAsIcon() const
{
	return m_pSettings->value(SETTING_USE_AVATAR_AS_ICON).toBool();
}

bool SettingsDialog::shouldAutoCheckForUpdates() const
{
	return m_pSettings->value(SETTING_AUTO_CHECK_FOR_UPDATES).toBool();
}

bool SettingsDialog::isDebugLoggingEnabled() const
{
	return m_pSettings->value(SETTING_ENABLE_DEBUG_LOGGING).toBool();
}

uint SettingsDialog::getTracksPerScrobble() const
{
	return m_pSettings->value(SETTING_TRACKS_PER_SCROBBLE).toUInt();
}

void SettingsDialog::requestAvatar()
{
	ui->avatarLabel->setPixmap(m_defaultUserPixmap);
	m_scrobbler.requestUserInfo(getUsername());
}

void SettingsDialog::onLoginButtonClicked()
{
	ui->usernameLineEdit->setEnabled(false);
	ui->passwordLineEdit->setEnabled(false);
	ui->loginButton->setEnabled(false);

	ui->statusLabel->setText(tr("Logging in..."));

	m_scrobbler.authenticate(ui->usernameLineEdit->text(), ui->passwordLineEdit->text());
}

void SettingsDialog::onLogout()
{
	ui->usernameLineEdit->setEnabled(true);
	ui->usernameLineEdit->clear();

	ui->passwordLineEdit->setEnabled(true);
	ui->passwordLineEdit->clear();

	ui->loginButton->setEnabled(true);

	ui->statusLabel->clear();

	ui->avatarLabel->setPixmap(m_defaultUserPixmap);

	ui->loggedInWidget->hide();
	ui->loggedOutWidget->show();
	ui->loginButton->setDefault(true);

	ui->usernameLineEdit->setFocus();

	saveSettings();
}

void SettingsDialog::onAuthenticationError(const QString &error)
{
	ui->usernameLineEdit->setEnabled(true);
	ui->passwordLineEdit->setEnabled(true);
	ui->loginButton->setEnabled(true);

	ui->statusLabel->setText(error);
}

void SettingsDialog::onAuthenticateSuccess()
{
	onAuthenticateSuccess(getUsername());
}

void SettingsDialog::onAuthenticateSuccess(const QString &username)
{
	saveSettings();

	requestAvatar();

	ui->loggedInLabel->setText(tr("Logged in as: %1").arg(username));
	ui->loggedOutWidget->hide();
	ui->loggedInWidget->show();
	ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
}

void SettingsDialog::onAvatar(const QPixmap &avatar)
{
	if (!avatar.isNull())
		ui->avatarLabel->setPixmap(avatar);
	else
		ui->avatarLabel->setPixmap(m_defaultUserPixmap);

	ui->avatarLabel->update(ui->avatarLabel->rect());
}

void SettingsDialog::loadSettings()
{
	// Default waitBeforeScrobble to 5 seconds
	if (!m_pSettings->contains(SETTING_WAIT_BEFORE_SCROBBLE))
		m_pSettings->setValue(SETTING_WAIT_BEFORE_SCROBBLE, 5);

	// Default auto check for updates to true
	if (!m_pSettings->contains(SETTING_AUTO_CHECK_FOR_UPDATES))
		m_pSettings->setValue(SETTING_AUTO_CHECK_FOR_UPDATES, Qt::Checked);

	// Default tracks per scrobble to 50
	if (!m_pSettings->contains(SETTING_TRACKS_PER_SCROBBLE))
		m_pSettings->setValue(SETTING_TRACKS_PER_SCROBBLE, 50);

	// Load settings from ini file...

	// User credentials
	ui->usernameLineEdit->setText(getUsername());
	ui->passwordLineEdit->clear();
	m_scrobbler.setSessionKey(m_pSettings->value(SETTING_SESSION_KEY).toString());

	// Start with Windows
	const QString executableFilename = LogWindow::getCurrentExecutablePath();
	const QString registryStartupPath = m_pRegistrySettings->value(REGISTRY_KEY_NAME).toString();

	if (registryStartupPath == executableFilename)
		ui->startWithWindowsCheckBox->setCheckState(Qt::Checked);
	else
		ui->startWithWindowsCheckBox->setCheckState(Qt::Unchecked);

	// Use avatar as icon
	ui->useAvatarAsIconCheckBox->setCheckState(static_cast<Qt::CheckState>(m_pSettings->value(SETTING_USE_AVATAR_AS_ICON).toUInt()));

	// Auto update
	ui->autoUpdateCheckBox->setCheckState(static_cast<Qt::CheckState>(m_pSettings->value(SETTING_AUTO_CHECK_FOR_UPDATES).toUInt()));

	// Enable debug logging
	ui->enableDebugLoggingCheckBox->setCheckState(static_cast<Qt::CheckState>(m_pSettings->value(SETTING_ENABLE_DEBUG_LOGGING).toUInt()));

	// Wait before scrobble
	ui->waitSpinBox->setValue(m_pSettings->value(SETTING_WAIT_BEFORE_SCROBBLE).toUInt());
}

void SettingsDialog::saveSettings()
{
	// User credentials
	m_pSettings->setValue(SETTING_USERNAME, ui->usernameLineEdit->text());
	m_pSettings->setValue(SETTING_SESSION_KEY, m_scrobbler.getSessionKey());

	// Old versions of RockScrobbler kept the password in the ini file.
	// We don't need it any more, so remove it if it is there.
	m_pSettings->remove(SETTING_PASSWORD);

	// Start with Windows
	const QString executableFilename = LogWindow::getCurrentExecutablePath();

	if (ui->startWithWindowsCheckBox->checkState() == Qt::Checked)
	{
		m_pRegistrySettings->setValue(REGISTRY_KEY_NAME, executableFilename);
	}
	else
	{
		const QString registryStartupPath = m_pRegistrySettings->value(REGISTRY_KEY_NAME).toString();
		if (registryStartupPath == executableFilename)
			m_pRegistrySettings->remove(REGISTRY_KEY_NAME);
	}

	// Use avatar as icon (emit a signal if it changes).
	const bool oldAvatarSetting = shouldUseAvatarAsIcon();
	m_pSettings->setValue(SETTING_USE_AVATAR_AS_ICON, ui->useAvatarAsIconCheckBox->checkState());
	if (shouldUseAvatarAsIcon() != oldAvatarSetting)
		emit useAvatarAsIconChanged(shouldUseAvatarAsIcon());

	// Auto update (emit a signal if it changes).
	const bool oldAutoUpdateSetting = shouldAutoCheckForUpdates();
	m_pSettings->setValue(SETTING_AUTO_CHECK_FOR_UPDATES, ui->autoUpdateCheckBox->checkState());
	if (shouldAutoCheckForUpdates() != oldAutoUpdateSetting)
		emit shouldAutoCheckForUpdatesChanged(shouldAutoCheckForUpdates());

	// Enable debug logging
	m_pSettings->setValue(SETTING_ENABLE_DEBUG_LOGGING, ui->enableDebugLoggingCheckBox->checkState());

	// Wait before scrobble
	m_pSettings->setValue(SETTING_WAIT_BEFORE_SCROBBLE, ui->waitSpinBox->value());
}
