QT += widgets xml network
CONFIG += openssl c++11

TARGET = RockScrobbler
TEMPLATE = app

SOURCES += main.cpp\
	BitBucketApi.cpp \
	DriveWatcher.cpp \
	LogWindow.cpp \
	Scrobbler.cpp \
	SettingsDialog.cpp

HEADERS += BitBucketApi.h \
	DriveWatcher.h \
	LogWindow.h \
	QtDir.h \
	Scrobbler.h \
	SettingsDialog.h \
	Version.h	\
	resource.h

FORMS += About.ui \
	LogWindow.ui \
	SettingsDialog.ui

RESOURCES += \
	Resources.qrc

win32 {
	DEFINES += NOMINMAX
	LIBS += wbemuuid.lib
}
